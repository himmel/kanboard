<?php

namespace Kanboard\Core\User;

use Kanboard\Core\Base;

/**
 * Group Synchronization
 *
 * @package  user
 * @author   Frederic Guillot
 */
class GroupSync extends Base
{
    /**
     * Synchronize group membership
     *
     * @access public
     * @param  integer  $userId
     * @param  string[] $externalGroupIds
     */
    public function synchronize($userId, array $externalGroupIds)
    {
        if (DEBUG) {
            $this->logger->debug(__METHOD__.': Group synchronization triggered');
        }

        $userGroups = $this->groupMemberModel->getGroups($userId);
        $this->addGroups($userId, $userGroups, $externalGroupIds[0]);
        $this->removeGroups($userId, $userGroups, $externalGroupIds[0]);
    }

    /**
     * Add missing groups to the user
     *
     * @access protected
     * @param integer  $userId
     * @param array    $userGroups
     * @param string[] $externalGroupNames
     */
    protected function addGroups($userId, array $userGroups, array $externalGroupNames)
    {
        $userGroupNames = array_column($userGroups, 'name');
        $localGroups = $this->groupModel->getAll();

        if (DEBUG) {
            $this->logger->debug(__METHOD__.': Current user groups are ['.implode(', ', $userGroupNames).']');
            $this->logger->debug(__METHOD__.': External groups are ['.implode(', ', $externalGroupNames).']');
        }

        foreach ($localGroups as $localGroup) {
            if (in_array($localGroup['name'], $externalGroupNames)) {
                if (! in_array($localGroup['name'], $userGroupNames)) {
                    if (DEBUG) {
                        $this->logger->debug(__METHOD__.': User is not in group '.$localGroup['name'].' yet, adding user to group');
                    }
                    $this->groupMemberModel->addUser($localGroup['id'], $userId);
                }
            }
        }
    }

    /**
     * Remove groups from the user
     *
     * @access protected
     * @param integer  $userId
     * @param array    $userGroups
     * @param string[] $externalGroupNames
     */
    protected function removeGroups($userId, array $userGroups, array $externalGroupNames)
    {
        $userGroupNames = array_column($userGroups, 'name');
        $localGroups = $this->groupModel->getAll();

        foreach ($localGroups as $localGroup) {
            if (! in_array($localGroup['name'], $externalGroupNames) && in_array($localGroup['name'], $userGroupNames)) {
                if (DEBUG) {
                    $this->logger->debug(__METHOD__.': User is still in group '.$localGroup['name'].', removing user from group');
                }
                $this->groupMemberModel->removeUser($localGroup['id'], $userId);
            }
        }
    }
}

/* vim: set ts=4 sw=4 expandtab: */
